

#!/bin/bash
sudo apt update -y
sudo apt dist-upgrade -y
sudo apt autoremove -y
sudo apt update -y 
sudo apt install wget curl vim elinks git unzip -y 
sudo apt-get install openjdk-8-jdk openjdk-8-doc -y
sudo apt install wget software-properties-common -y 
wget -qO - https://api.bintray.com/orgs/jfrog/keys/gpg/public.key | apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://jfrog.bintray.com/artifactory-debs $(lsb_release -cs) main" 
sudo apt update -y 
sudo apt install jfrog-artifactory-oss -y 
systemctl stop artifactory.service
systemctl start artifactory.service
systemctl enable artifactory.service